package com.vblago.Threads;

import com.vblago.util.Generator;

import static com.vblago.ui.Main.cities;

public class RenewalWeather implements Runnable {
    private int timeSleep;

    public RenewalWeather(int timeSleep) {
        this.timeSleep = timeSleep;
    }

    @Override
    public void run() {
        for (int i = 0; i < 60 / timeSleep; i++) {
            for (String key : cities.keySet()) {
                cities.put(key, Generator.getNewWeather());
            }

            try {
                Thread.sleep(timeSleep * 1000);
            } catch (InterruptedException ignored) {
            }
        }
    }


}
