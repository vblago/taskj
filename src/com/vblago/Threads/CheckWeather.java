package com.vblago.Threads;

import com.vblago.ui.Main;

import static com.vblago.ui.Main.cities;

public class CheckWeather implements Runnable {
    private int timeSleep;
    private String[] language;

    public CheckWeather(int timeSleep, String[] language) {
        this.timeSleep = timeSleep;
        this.language = language;
    }

    @Override
    public void run() {
        for (int i = 0; i < (60 / timeSleep); i++) {
            synchronized (Main.class){
                for (String key : cities.keySet()) {
                    System.out.println(key + cities.get(key).toString(language));
                }
                System.out.println();
            }

            try {
                Thread.sleep(timeSleep * 1000);
            } catch (InterruptedException ignored) {
            }
        }
    }

}
