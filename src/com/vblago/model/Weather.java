package com.vblago.model;

public class Weather {
    private float temperature;
    private float pressure;
    private float humidity;

    public Weather(float temperature, float pressure, float humidity) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
    }

    public String toString(String lang[]) {
        return String.format(" %s: %.2f, %s %.2f, %s %.2f",
                lang[0], temperature, lang[1], pressure, lang[2], humidity);
    }
}
