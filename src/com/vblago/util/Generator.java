package com.vblago.util;

import com.vblago.model.Weather;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Generator {
    public static Map<String, Weather> getCities(){
        Map<String, Weather> cities = new ConcurrentHashMap<>();

        cities.put("Kharkiv", new Weather(genTemp(), genPress(), genHum()));
        cities.put("Moscow", new Weather(genTemp(), genPress(), genHum()));
        cities.put("London", new Weather(genTemp(), genPress(), genHum()));

        return cities;
    }

    public static Map<String, String[]> genLanguages(){
        Map<String, String[]> langCity = new HashMap<>();

        langCity.put("Kharkiv", new String[]{"температура", "тиск", "вологість"});
        langCity.put("Moscow", new String[]{"температура", "давление", "влажность"});
        langCity.put("London", new String[]{"temperature", "pressure", "humidity"});

        return langCity;
    }

    public static int[] genFrequencies(){
        int[] freq = new int[4];
        freq[0] = 10;
        freq[1] = 5;
        freq[2] = 15;
        freq[3] = 25;
        return freq;
    }

    public static Weather getNewWeather(){
        return new Weather(genTemp(), genPress(), genHum());
    }

    private static float genTemp(){
        return -30 + (float) Math.random() * 75;
    }

    private static float genPress(){
        return 730 + (float) Math.random() * 60;
    }

    private static float genHum(){
        return (float) Math.random() * 100;
    }
}
