package com.vblago.ui;

import com.vblago.Threads.CheckWeather;
import com.vblago.Threads.RenewalWeather;
import com.vblago.model.Weather;
import com.vblago.util.Generator;

import java.util.Iterator;
import java.util.Map;

public class Main {

    public static Map<String, Weather> cities;

    public static void main(String[] args) {
        cities = Generator.getCities();
        Map<String, String[]> languages = Generator.genLanguages();
        int[] frequencies = Generator.genFrequencies();

        Thread threadAdd = new Thread(new RenewalWeather(frequencies[0]));
        threadAdd.start();

        Thread threadCheck[] = new Thread[3];
        Iterator<String[]> iter = languages.values().iterator();
        for (int i = 0; i<3; i++){
            threadCheck[i] = new Thread(new CheckWeather(frequencies[i+1], iter.next()));
            threadCheck[i].start();
        }

    }
}
